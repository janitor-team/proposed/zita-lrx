//  ------------------------------------------------------------------------
//
//  Copyright (C) 2011-2012 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  ------------------------------------------------------------------------


#ifndef __LRXCONF_H
#define __LRXCONF_H


#include "global.h"


class Channel
{
public:

    Channel (void);
    ~Channel (void);

    char   _label [16];
    float  _gain;
    float  _delay;
    char  *_dest [MAXBAND];
};
 

class Fband
{
public:

    char   _label [16];
    float  _gain;
    float  _delay;
};


class Xover
{
public:

    float  _freq;
    float  _shape;
};
  

class Lrxconf
{
public:

    Lrxconf (void);
    ~Lrxconf (void);

    friend class Jclient;
    friend class Lrxproc;

    const char *filename (void) const { return _filename; }

    int  load (const char *file);
    int  save (const char *file);
    int  check (float fsamp);

private:

    enum { NOERR, ERROR, COMM, ARGS, EXTRA, SCOPE };

    void reset (void);
    void init (int nband, int nchan);

    char      _filename [1024];
    int       _nband;
    int       _nchan;
    Channel  *_chann;
    Fband     _fband [MAXBAND];
    Xover     _xover [MAXFILT];
};    


#endif
