//  ------------------------------------------------------------------------
//
//  Copyright (C) 2009-2012 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  ------------------------------------------------------------------------


#ifndef __LR4FILT_H
#define __LR4FILT_H


// State variable version of 4th order crossover filters.
// The 'f' parameter is the crossover frequency divided by
// the sample rate. The 's' parameter defines the filter
// shape, s = 0 for Linkwitz-Riley, s = 1 for Butterworth.
// Outputs are exactly in phase.


class LR4_conf
{
public:

    LR4_conf (void) : _gl (0), _gh (0), _c1 (0), _c2 (0), _c3 (0), _c4 (0)  {};
    ~LR4_conf (void) {}

    void calcpar (float f, float s);

private:

    friend class LR4_filt;

    float _gl, _gh;  // LP and HP gain factors.
    float _c1, _c2;  // First stage coefficients.
    float _c3, _c4;  // Second stage coefficients.
};


class LR4_filt
{
public:

    LR4_filt (void) { reset (); }
    ~LR4_filt (void) {}

    void reset (void) { _z1 = _z2 = _z3 = _z4 = _z5 = _z6 = 0; }
    void process (const LR4_conf *C, int n, float glo, float ghi, float *inp, float *opl, float *oph);

private:

    float _z1, _z2, _z3, _z4, _z5, _z6;  // filter state 
};


#endif
